#include <iostream>          
#include "opencv2/opencv.hpp"
#include "PracticalSocket.h" 
#include "argparser.hpp"
#include "utils.h"


using namespace cv;

//server config
#define PACK_SIZE 4096 //udp pack size; note that OSX limits < 8100 bytes
#define BUF_LEN 65540 // Larger than maximum UDP packet size


int main ( int argc, char * argv[] )
{
    ap::parser p ( argc, argv );
    p.add ( "", "--port",  "port to use",     ap::mode::REQUIRED );
    p.add ( "", "--mode", "mode to use: live|save, default is live\n\tlive : display images\n\tsave : save images to disk" );
    p.add ( "", "--max-save",  "max number of images to save, default is 100" );

    auto args = p.parse();

    if ( !args.parsed_successfully() )
    {
        std::cerr << "Wrong arguments!\n";
        return -1;
    }

    if ( !args["--mode"].empty() && args["--mode"] != "live" && args["--mode"] != "save" )
    {
        std::cerr << args["--mode"] << " is an invalid mode! mode must be live or save\n";
        return -1;
    }

    const auto servPort = std::stoi ( args["--port"] ); //local port
    const auto mode = args["--mode"].empty() ? "live" : args["--mode"];
    const auto maxImages = args["--max-save"].empty() ? 100 : std::stoi ( args["--max-save"] );
    
    const std::string windowName = "Received";
    if(mode == "live")
	cv::namedWindow ( windowName, cv::WINDOW_NORMAL );
    
    int imageIndex = 0;
    
    try
    {
        UDPSocket sock ( servPort );

        char buffer[BUF_LEN]; // Buffer for echo string
        int recvMsgSize; // Size of received message
        std::string sourceAddress; // Address of datagram source
        unsigned short sourcePort; // Port of datagram source

        clock_t last_cycle = clock();

        while ( 1 )
        {
            // Block until receive message from a client
            do
            {
                std::cerr << "Waiting for messages from a client\n";
                recvMsgSize = sock.recvFrom ( buffer, BUF_LEN, sourceAddress, sourcePort );
            }
            while ( recvMsgSize > sizeof ( int ) );
	    
	    
            int total_pack = ( ( int * ) buffer ) [0];
            //std::cout << "Expecting length of packs:" << total_pack << std::endl;
	    
            char * longbuf = new char[PACK_SIZE * total_pack];
            for ( int i = 0; i < total_pack; i++ )
            {
                recvMsgSize = sock.recvFrom ( buffer, BUF_LEN, sourceAddress, sourcePort );
                if ( recvMsgSize != PACK_SIZE )
                {
                    std::cerr << "Received unexpected size pack:" << recvMsgSize << std::endl;
                    continue;
                }
                memcpy ( & longbuf[i * PACK_SIZE], buffer, PACK_SIZE );
            }

            cout << "Received packet from " << sourceAddress << ":" << sourcePort << endl;

            Mat rawData = Mat ( 1, PACK_SIZE * total_pack, CV_8UC1, longbuf );
            Mat frame = imdecode ( rawData, CV_LOAD_IMAGE_COLOR ).clone();
	    free ( longbuf );
	    
            if ( frame.size().width == 0 )
            {
                std::cerr << "decode failure!" << std::endl;
                continue;
            }
                 
            clock_t next_cycle = clock();
            double duration = ( next_cycle - last_cycle ) / ( double ) CLOCKS_PER_SEC;
            cout << "\tEffective FPS:" << ( 1 / duration ) << " \tkbps:" << ( PACK_SIZE * total_pack / duration / 1024 * 8 ) << std::endl;

            last_cycle = next_cycle;
	    
            saveDisplayImage (frame, windowName, mode, imageIndex, maxImages);
	    cv::waitKey ( 30 );   //DELAY_CAPTION milliseconds	    
        }
    }
    catch ( SocketException & e )
    {
        std::cerr << e.what() << std::endl;
        std::exit ( 1 );
    }

    return 0;
}

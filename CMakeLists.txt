cmake_minimum_required( VERSION 2.8 )
project(FRC_2017_Vision)

# guard against in-source builds
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.")
endif()

#check c++11 support
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
if(COMPILER_SUPPORTS_CXX11)
        # enable C++11 support
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
else()
        message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()


find_package( OpenCV REQUIRED )

#root directory
set(ROOT_DIR  ${CMAKE_CURRENT_SOURCE_DIR})

#Binary output directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${ROOT_DIR}/bin)


include_directories( ${ROOT_DIR} )
include_directories( ${ROOT_DIR}/client  )
include_directories( ${ROOT_DIR}/thirdParty  )


file(GLOB CLIENT_SRC ${ROOT_DIR}/client/*.cpp ${ROOT_DIR}/thirdParty/*.cpp)
file(GLOB SERVER_SRC ${ROOT_DIR}/server/*.cpp ${ROOT_DIR}/client/utils.cpp ${ROOT_DIR}/thirdParty/*.cpp)



add_executable(frcVisionClient  ${CLIENT_SRC})
target_link_libraries( frcVisionClient pthread  ${OpenCV_LIBS})

add_executable(frcVisionServer ${SERVER_SRC})
target_link_libraries(frcVisionServer pthread  ${OpenCV_LIBS})


import numpy as np
import cv2

cap = cv2.VideoCapture(0)

if cap.isOpened():
    print "cap opened"
    ret, frame = cap.read()
    cv2.imwrite('capture.png', frame)
else:
    print "Error"

cap.release()

#include <iostream>
#include <fstream>

#include "camprocessor.h"
#include "argparser.hpp"

using namespace std;


int main ( int argc, char** argv )
{
    ap::parser p ( argc, argv );
    p.add ( "", "--server",  "Server adress", ap::mode::REQUIRED );
    p.add ( "", "--port",  "Server port", ap::mode::REQUIRED );
    p.add ( "", "--cam1",  "first cam index, cam index or video path, default is 0" );
    p.add ( "", "--cam2",  "second cam, cam index or video path, default is 1" );
    p.add ( "", "--width",  "image width, default 640" );
    p.add ( "", "--height",  "image height, default 480" );
    //p.add ( "", "--fps",  "Number of frame per second, default 33" );
    p.add ( "", "--delay",  "Caption delay in milliseconds, default is 33" );
    p.add ( "", "--quality",  "jpeg encoding quality, default 80" );
    p.add ( "", "--mode", "mode to use: live|save\n\tlive : display images\n\tsave : save images to disk" );

    auto args = p.parse();

    if ( !args.parsed_successfully() )
    {
        std::cerr << "Wrong arguments!\n";
        return -1;
    }

    const auto server =  args["--server"];
    const auto port =  args["--port"];
    const auto cam1 = args["--cam1"].empty() ? "0" : args["--cam1"];
    const auto cam2 = args["--cam2"].empty() ? "1" : args["--cam2"];
    const auto mode =  args["--mode"];

    const auto width  = args["--width"].empty() ? 640 : std::stoi ( args["--width"] );
    const auto height = args["--height"].empty() ? 480 : std::stoi ( args["--height"] );
    const auto quality    = args["--quality"].empty() ? 80 : std::stoi ( args["--quality"] );

    //const auto fps    = args["--fps"].empty() ? 33 : std::stoi ( args["--fps"] );
    const auto delay    = args["--delay"].empty() ? 33 : std::stoi ( args["--delay"] );

    CamProcessor camprocessor ( cam1, cam2 );

    //camprocessor.setCaptureSettings(width, height, fps);
    camprocessor.setSendSettings ( width, height, quality );

    camprocessor.start ( server, port, mode, delay );


    return 0;
}


#include "camprocessor.h"
#include "PracticalSocket.h"

#include <opencv2/video/video.hpp>

#include <iostream>
#include <memory>

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

CamProcessor::CamProcessor ( const std::string& cam1, const std::string& cam2 )
{
    cam1Cap = CamCapture ( cam1 );
    cam2Cap = CamCapture ( cam2 );
}

CamProcessor::~CamProcessor()
{

}

void CamProcessor::setCaptureSettings ( const int w, const int h, const int fps )
{
    cam1Cap.cv_capture.set ( CV_CAP_PROP_FRAME_WIDTH, w );
    cam1Cap.cv_capture.set ( CV_CAP_PROP_FRAME_HEIGHT, h );
    cam1Cap.cv_capture.set ( CV_CAP_PROP_FPS, fps );

    cam2Cap.cv_capture.set ( CV_CAP_PROP_FRAME_WIDTH, w );
    cam2Cap.cv_capture.set ( CV_CAP_PROP_FRAME_HEIGHT, h );
    cam2Cap.cv_capture.set ( CV_CAP_PROP_FPS, fps );

    std::cout << "-------------------------"<< std::endl;
    std::cout << "Set capture width to "<< w <<std::endl;
    std::cout << "Set capture height to "<< h <<std::endl;
    std::cout << "Set capture fps to "<< fps <<std::endl;
    std::cout << "-------------------------"<< std::endl;
}

void CamProcessor::setSendSettings ( const int width, const int height, const int quality )
{
    FRAME_SEND_WIDTH = width;
    FRAME_SEND_HEIGHT = height;
    ENCODE_QUALITY = quality;

    if ( !cam1Cap.noCamFound.empty() )
        cv::resize ( cam1Cap.noCamFound, cam1Cap.noCamFound, cv::Size ( width, height ) );
    if ( !cam2Cap.noCamFound.empty() )
        cv::resize ( cam2Cap.noCamFound, cam2Cap.noCamFound, cv::Size ( width, height ) );

    std::cout << "-------------------------"<< std::endl;
    std::cout << "Set send frame width to "<< width <<std::endl;
    std::cout << "Set send framet height to "<< height <<std::endl;
    std::cout << "Set send encode quality to "<< quality <<std::endl;
    std::cout << "-------------------------"<< std::endl;
}

int CamProcessor::sendFrame ( const cv::Mat& frame, const std::string& server, const std::string& port )
{
    unsigned short servPort = Socket::resolveService ( port, "udp" );

    try
    {
        UDPSocket sock;

        vector < uchar > encoded;
        cv::imencode ( ".jpg", frame, encoded, {CV_IMWRITE_JPEG_QUALITY, ENCODE_QUALITY} );
        int total_pack = 1 + ( encoded.size() - 1 ) / PACK_SIZE;

        int ibuf[1];
        ibuf[0] = total_pack;

        sock.sendTo ( ibuf, sizeof ( int ), server, servPort );

        for ( int i = 0; i < total_pack; i++ )
            sock.sendTo ( & encoded[i * PACK_SIZE], PACK_SIZE, server, servPort );

    }
    catch ( SocketException & e )
    {
        std::cerr << e.what() << endl;
    }

    return 0;
}

void CamProcessor::start ( const std::string& server, const std::string& port, const std::string mode, const int delay )
{
    const std::string windowName = "DRVImage";
    if ( mode == "live" )
        cv::namedWindow ( windowName, cv::WINDOW_NORMAL );


    std::vector<cv::Mat> frames = std::vector<cv::Mat> ( 2 );
    std::vector<CamCapture*> camCaptures = {&cam1Cap, &cam2Cap};

    cv::Mat fg_mask ;
    std::vector<std::unique_ptr<cv::BackgroundSubtractorMOG2>> backgroundSubtractor;
    backgroundSubtractor.push_back( make_unique<cv::BackgroundSubtractorMOG2>() );
    backgroundSubtractor.push_back( make_unique<cv::BackgroundSubtractorMOG2>() );

    int imageIndex = 0;

    bool nonStop = true;
    while ( nonStop )
    {
        for ( int i ( 0 ); i < 2; i++ )
        {
            if ( !camCaptures[i]->getFrame ( frames[i] ) )
            {
                nonStop = false;
                break;
            }

            else if ( camCaptures[i]->isValidCamera() )
            {
                cv::Mat frame_gray;
                cv::cvtColor ( frames[i], frame_gray, cv::COLOR_BGR2GRAY );

                (*backgroundSubtractor[i]) ( frame_gray, fg_mask );

                cv::threshold ( fg_mask, fg_mask, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU );
                cv::erode ( fg_mask, fg_mask, cv::getStructuringElement ( cv::MORPH_ELLIPSE, cv::Size ( 3, 3 ) ) );
                cv::dilate ( fg_mask, fg_mask, cv::getStructuringElement ( cv::MORPH_ELLIPSE, cv::Size ( 3, 3 ) ) );

                std::vector<cv::Rect> coutours_bbox = find_coutours ( fg_mask, fg_mask.cols /10 );
                std::vector<TargetType> targetsType = getTargetType ( frame_gray, coutours_bbox );

                std::string text = "Searching for target";
                cv::Scalar color = BLACK;

                const auto targetAcquired = [] ( TargetType t )
                {
                    return t == TARGET_ACQUIRED;
                };

                const auto targetInRange = [] ( TargetType t )
                {
                    return t == TARGET_IN_RANGE;
                };

                if ( std::any_of ( targetsType.begin(), targetsType.end(), targetAcquired ) )
                {
                    text = "Target Lock Acquired";
                    color = RED;
                }
                else if ( std::any_of ( targetsType.begin(), targetsType.end(), targetInRange ) )
                {
                    text = "Target in range";
                    color = YELLOW;
                }

                drawAxis ( frames[i], color );
                drawRectangles ( frames[i], coutours_bbox, targetsType );
                drawArrows ( frames[i], coutours_bbox );

                cv::resize ( frames[i], frames[i], cv::Size ( FRAME_SEND_WIDTH, FRAME_SEND_HEIGHT ) );
                drawText ( frames[i], text, color );
            }
        }

        if ( !frames[0].empty() &&  !frames[1].empty() )
        {
            //Concates both images into a single image
            cv::Mat DRVImage;
            cv::hconcat ( frames[0],  frames[1], DRVImage );
            saveDisplayImage ( DRVImage, windowName, mode, imageIndex, maxImagesToSave );
            saveDisplayImage ( fg_mask, "fg_mask", mode, imageIndex, maxImagesToSave );

            int codereturn = this->sendFrame ( DRVImage, server, port );
            cv::waitKey ( delay );   //DELAY_CAPTION milliseconds
        }
    }
}


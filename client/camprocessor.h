#ifndef CAMPROCESSOR_H
#define CAMPROCESSOR_H

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "utils.h"

#define PACK_SIZE 4096 //udp pack size; note that OSX limits < 8100 bytes


#define DEFAULT_FRAME_SEND_WIDTH 640 
#define DEFAULT_FRAME_SEND_HEIGHT 480 
    
    
struct CamCapture
{
    cv::VideoCapture cv_capture;
    cv::Mat noCamFound;
    bool validCamera = true;
    
    CamCapture()
    {
      
    }
    CamCapture ( const std::string& input )
    {
        is_number ( input ) ? cv_capture.open ( std::stoi ( input ) ) :  cv_capture.open ( input ) ;

        if ( !cv_capture.isOpened() ) // check if we succeeded
        {
            std::cerr << "Cannot open input "<< input << std::endl;
            validCamera = false;
            noCamFound = readImage ( "../nocamera.jpg" );
	    cv::resize ( noCamFound, noCamFound, cv::Size( DEFAULT_FRAME_SEND_WIDTH , DEFAULT_FRAME_SEND_HEIGHT) );
        }
        else
	{
	    std::cout << "-------------------------"<< std::endl;
	    std::cout << "Default Capture setting for input "<< input<< std::endl;
	    std::cout << "fps : " << cv_capture.get(CV_CAP_PROP_FPS) << std::endl;
	    std::cout << "FRAME_WIDTH : " << cv_capture.get(CV_CAP_PROP_FRAME_WIDTH) << std::endl;
	    std::cout << "FRAME_HEIGHT : " << cv_capture.get(CV_CAP_PROP_FRAME_HEIGHT) << std::endl;
	    std::cout << "-------------------------"<< std::endl;
	}
    }
    bool getFrame(cv::Mat& frame)
    {
        if ( validCamera )
        {
            return cv_capture.read ( frame );
        }
        else
        {
	    frame = noCamFound;
            return true;
        }
    }
    bool isValidCamera()
    {
      return validCamera;
    }
};

class CamProcessor
{
    CamCapture cam1Cap, cam2Cap ;

    int maxImagesToSave = 100;
    std::string mode = "";

    int FRAME_SEND_WIDTH  = DEFAULT_FRAME_SEND_WIDTH ;
    int FRAME_SEND_HEIGHT = DEFAULT_FRAME_SEND_HEIGHT ;
    int ENCODE_QUALITY    = 80; //JPEG Quality

private:
    int sendFrame ( const cv::Mat& frame, const std::string& server, const std::string& port );

public:
    CamProcessor ( const std::string& cam1, const std::string& cam2 );
    ~CamProcessor();

    void setCaptureSettings ( const int w, const int h, const int fps );
    void setSendSettings ( const int width, const int height, const int quality );
    
    void start ( const std::string& server, const std::string& port, const std::string mode = "", const int delay = 33);

};

#endif // CAMPROCESSOR_H

#include "utils.h"
#include <iostream>



bool is_number ( const std::string& s )
{
    return !s.empty() && std::find_if ( s.begin(),
                                        s.end(), [] ( char c )
    {
        return !std::isdigit ( c );
    } ) == s.end();
}


cv::Mat readImage ( const std::string& path )
{
    cv::Mat image = cv::imread ( path );
    if ( !image.data )
    {
        std::cerr <<  "Could not open "<<  path << std::endl ;
        std::exit ( -1 );
    }
    return image;
}


void saveDisplayImage ( const cv::Mat& frame, const std::string& WinTitle, const std::string& mode, int& imageIndex, const int maxToSave )
{
    if ( mode == "live" )
    {
        cv::imshow ( WinTitle, frame );
    }
    else if ( mode == "save" && imageIndex++ < maxToSave )
    {
        std::string indexString = std::to_string ( imageIndex );
        indexString = std::string ( 3 - indexString.length(), '0' ) + indexString;
        cv::imwrite ( WinTitle + "-" + indexString + ".png", frame );
    }
}


std::vector<cv::Rect> find_coutours ( cv::Mat& img, int minArea )
{
    std::vector<cv::Rect> targets;

    /// Find contours
    cv::vector<cv::vector<cv::Point> > contours;
    cv::vector<cv::Vec4i> hierarchy;
    cv::findContours ( img.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point ( 0, 0 ) );

    for ( int i = 0; i< contours.size(); i++ )
    {
        double area = cv::contourArea ( contours[i], false );
        if ( area > minArea )
        {
            targets.push_back ( cv::boundingRect ( contours[i] ) );
        }
    }

    //return targets;
    return mergeOverlappingRectangles ( targets );
}

std::vector< cv::Rect > mergeOverlappingRectangles ( const std::vector< cv::Rect >& rects )
{
    std::vector<cv::Rect> newRects = rects;
    bool moreRectsToMerge = true;

    while ( moreRectsToMerge )
    {
        for ( int i ( 0 ); i < newRects.size(); i++ )
        {
            for ( int j ( 0 ); j < newRects.size(), i!=j; j++ )
            {
                if ( newRects[i].area() > 0 && newRects[j].area() > 0 )
                {
                    cv::Rect intersection = newRects[i] & newRects[j];

                    if ( intersection.area() > 0 )
                    {
                        if ( intersection.area() == newRects[j].area() )
                        {
                            newRects[j] = cv::Rect();
                        }
                        else if ( intersection.area() == newRects[i].area() )
                        {
                            newRects[i] = cv::Rect();
                        }
                        else
                        {
                            newRects[i] =  newRects[i] | newRects[j];
                            newRects[j] = cv::Rect();
                        }
                    }
                }
            }
        }
        moreRectsToMerge = false;
        newRects.erase ( std::remove_if ( newRects.begin(), newRects.end(), [&] ( const cv::Rect r )-> bool { return r.area() == 0; } ), newRects.end() );

        for ( int i ( 0 ); i < newRects.size(); i++ )
        {
            for ( int j ( 0 ); j < newRects.size(), i!=j; j++ )
            {
                cv::Rect intersection = newRects[i] & newRects[j];

                if ( intersection.area() > 0 )
                {
                    moreRectsToMerge = true;
                    goto exitLoop;
                }
            }
        }
    exitLoop:
        continue;
    }

    return newRects;
}


void drawText ( cv::Mat& frame, const std::string& text, cv::Scalar color )
{
    int thickness = 2;
    int font = cv::FONT_ITALIC;
    cv::Point org = cv::Point ( 10, 40 );

    cv::Size textSize = cv::getTextSize ( text, font, 1, thickness, nullptr );
    double scale = ( double ) frame.cols / ( ( double ) 3*textSize.width );

    cv::putText ( frame, text, scale*org, font, scale, color, thickness );
}
void drawAxis ( cv::Mat& frame, cv::Scalar color )
{
    cv::line ( frame, cv::Point ( 0, frame.rows/2 ), cv::Point ( frame.cols, frame.rows/2 ), color, 2 );
    cv::line ( frame, cv::Point ( frame.cols/2 , 0 ), cv::Point ( frame.cols/2 , frame.rows ), color, 2 );
}

void drawRectangles ( cv::Mat& frame, const std::vector<cv::Rect>& rects, const std::vector<TargetType>& targetsType )
{
    for ( int i ( 0 ); i < rects.size(); i++ )
    {
        cv::Scalar color = BLACK;
        if ( targetsType[i] == TARGET_IN_RANGE )
            color = YELLOW;
        else if ( targetsType[i] == TARGET_ACQUIRED )
            color = RED;

        cv::rectangle ( frame, rects[i], color, 2 );
    }
}

void drawArrows ( cv::Mat& frame, const std::vector<cv::Rect>& rects )
{
    for ( const auto& rect : rects )
    {
        cv::line ( frame, cv::Point ( frame.cols/2, frame.rows/2 ), cv::Point ( rect.x + rect.width /2 , rect.y + rect.height /2 ), GREEN, 2 );
    }
}

std::vector<TargetType> getTargetType ( const cv::Mat& frame, const std::vector< cv::Rect >& rects )
{
    cv::Point2f center = cv::Point2f ( frame.cols/2, frame.rows/2 );

    std::vector<TargetType> targetsType = std::vector<TargetType> ( rects.size(), TARGET_SEARCH );

    for ( int i ( 0 ); i < rects.size(); i++ )
    {
        int minAcquiredDistance = rects[i].width/2;

        if ( rects[i].x < center.x && rects[i].x + rects[i].width > center.x && rects[i].y < center.y && rects[i].y + rects[i].height > center.y )
        {
            targetsType[i] = TARGET_IN_RANGE;
            if ( cv::norm ( cv::Point2f ( rects[i].x + rects[i].width/2 , rects[i].y + rects[i].height/2 ) - center ) <  minAcquiredDistance )
            {
                targetsType[i]  = TARGET_ACQUIRED;
            }
        }
    }

    return targetsType;
}



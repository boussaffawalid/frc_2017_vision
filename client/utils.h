#ifndef UTILS_H
#define UTILS_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

enum TargetType {TARGET_SEARCH, TARGET_IN_RANGE, TARGET_ACQUIRED};


const cv::Scalar RED = CV_RGB ( 255, 0, 0 );
const cv::Scalar GREEN = CV_RGB ( 0, 255, 0 );
const cv::Scalar YELLOW = CV_RGB ( 255, 255, 0 );
const cv::Scalar BLUE = CV_RGB ( 0, 0, 255 );
const cv::Scalar BLACK = CV_RGB ( 0, 0, 0 );

cv::Mat readImage ( const std::string& path );

bool is_number ( const std::string& s );

void saveDisplayImage (const cv::Mat& frame, const std::string& WinTitle, const std::string& mode, int& imageIndex, const int maxToSave );

std::vector<cv::Rect> find_coutours ( cv::Mat& img, int minArea);


void drawText ( cv::Mat& frame, const std::string& text, cv::Scalar color );
void drawAxis ( cv::Mat& frame, cv::Scalar color );
void drawRectangles(cv::Mat& frame, const std::vector<cv::Rect>& rects, const std::vector<TargetType>& targetsType);
void drawArrows ( cv::Mat& frame, const std::vector<cv::Rect>& rects);

std::vector<cv::Rect> mergeOverlappingRectangles(const std::vector<cv::Rect>& rects);
std::vector<TargetType> getTargetType(const cv::Mat& frame, const std::vector<cv::Rect>& rects);

#endif // UTILS_H

#Project structure

├── bin
│   ├── frcVisionClient
│   └── frcVisionServer
├── build
├── client
│   ├── camprocessor.cpp
│   ├── camprocessor.h
│   ├── main.cpp
│   ├── utils.cpp
│   └── utils.h
├── CMakeLists.txt
├── config.h
├── README.md
├── server
│   └── main.cpp
├── setup_notes.txt
├── testCam.py
└── thirdParty
    ├── argparser.hpp
    ├── PracticalSocket.cpp
    └── PracticalSocket.h


#Install dependencies

sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install cmake libopencv-dev

#How to build

mkdir build
cd build
cmake ..
make

#How to use

On the Raspberry Pi:
    cd bin/
    ./frcVisionClient --server 192.168.161.54 --port 5000
Replace 192.168.161.54 with your machine ip adress

On your machine: 
    cd bin/
    ./frcVisionServer --port 5000

For more options:

./frcVisionClient -h
./frcVisionServer -h

